import './Footer.css';

export default function Footer() {
  return (
    <footer className="Footer">
      Copyright &copy;{' '}
      <a rel="noreferrer" href="http://www.yoursitename.com" target="_blank">
        www.yoursitename.com
      </a>{' '}
      - All Rights Reserved. Designed by{' '}
      <a rel="noreferrer" href="http://www.smartweeby.com" target="_blank">
        SmartWeeby.com
      </a>
    </footer>
  );
}
