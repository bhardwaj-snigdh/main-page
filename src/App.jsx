import { Container, Row } from 'reactstrap';
import Header from './components/Header';
import Sidebar from './components/Sidebar';
import Main from './components/Main';
import Footer from './components/Footer';

import './App.css';

function App() {
  return (
    <div className="App">
      <Header />
      <Container className="App__Container" fluid>
        <Row className="App__ContainerRow">
          <Sidebar md={3} xs={12} />
          <Main md={9} xs={12} />
        </Row>
      </Container>
      <Footer />
    </div>
  );
}

export default App;
